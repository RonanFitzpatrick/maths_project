/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maths_project_encryption;


import java.io.UnsupportedEncodingException;

import java.util.ArrayList;

/**
 *
 * @author ronan
 */
public class Maths_Project_Encryption
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
       
        
        long prime_one = generate_prime();
        long prime_two = generate_prime();
        long N = prime_one*prime_two;
        System.out.println("N="+N);
        long M = get_phi(N);
        System.out.println("M="+M);
        long E = 65537;
        long D = extended_euclidean_algorithim(M,E);
        System.out.println("D="+D);
        String plain_text = "test test test test";
        String encrypt = "";
        ArrayList<Long> list = new ArrayList();
        long num;
        for(byte b: plain_text.getBytes())
        {
            num = (long)b;
            list.add(pow_mod(num,E,N));
            
        }
        for(Long l: list)
        {
            
            
            encrypt += l;
           
        }
        
        int decrpyted_num;
        String decrypted = "";
        for(Long l: list)
        {
            
            decrpyted_num = (int)pow_mod(l,D,N);
            decrypted += (Character.toChars(decrpyted_num)[0]);
           
        }
        
        System.out.println("Plain text =" + plain_text);
        System.out.println("Encrypted = " + encrypt);
        System.out.println("Decrypted = " + decrypted);
        
        
       

       
        
    }

    
    


    public static long get_phi(long large)
    {

        long phi = large;
        for (int i = 2; i * i <= large; i++)
        {
            if (large % i == 0)
            {
                phi =phi/ i;
                phi = phi*(i - 1);
                while (large % i == 0)
                {
                    large = large/ i;
                }
            }
        }
        if (large > 1)
        {
            phi /= large;
            phi = phi* (large - 1);
        }

        return phi;
    }

    
    
    @SuppressWarnings("empty-statement")
    public static long generate_prime()
    {
        long generated_prime = 0;
        while (!isPrime(generated_prime = (long) (Math.random() * 46339)+1));
        return generated_prime;
    }
    
    

    public static boolean isPrime(long n)
    {
        if (n % 2 == 0)
        {
            return false;
        }
        if (n % 3 == 0)
        {
            return false;
        }
        for (long i = Math.round(n / 2); i > 2; i--)
        {
            if (n % i == 0)
            {
                return false;
            }
        }
        return true;
    }

    private static boolean coprime_check(long coprime_1, long coprime_2)
    {
        while (coprime_1 != 0 && coprime_2 != 0)
        {
            if (coprime_1 > coprime_2)
            {
                coprime_1 = coprime_1 % coprime_2;
            } else
            {
                coprime_2 = coprime_2 % coprime_1;
            }
        }
        return Math.max(coprime_1, coprime_2) == 1;
    }

    private static long extended_euclidean_algorithim(long big_number, long divisor)
    {
        
        if (coprime_check(big_number, divisor))
        {
            long world = big_number;
            long remainder = big_number % divisor;
            long row_one_top = 1;
            long row_two_top = 0;
            long row_one_bottom = 0;
            long row_two_bottom = 1;
            long temp;
            long temp2;
            long multiplier;
            //calculation loop
            while (remainder != 0)
            {
                //saving the bottom row before changing it
                temp = row_one_bottom;
                temp2 = row_two_bottom;

                //how many times whole times it goes in
                multiplier = (big_number - remainder) / divisor;

                //calculates the next line of the equation
                row_one_bottom = row_one_top + (row_one_bottom * ((-1) * (multiplier)));
                row_two_bottom = row_two_top + (row_two_bottom * ((-1) * (multiplier)));

                //makes the old bottom row the top row
                row_one_top = temp;
                row_two_top = temp2;

                //moves down to the next line of the method
                big_number = divisor;
                divisor = remainder;
                remainder = big_number % divisor;

            }
        //end of calculation

            //deals with negative numbers
            while (row_two_bottom < 0)
            {
                row_two_bottom = row_two_bottom + world;
            }

            return row_two_bottom;
        }
        return 0;
    }

    //my check for highest nums that together will overflow
    @Deprecated
    public static void check()
    {
        for (int i = 0;; i++)
        {
            long j = i * i;
            System.out.println(i + ":" + j);
            if (j < 0)
            {
                break;
            }
        }

    }
    
    
    public static long pow_mod (long base, long power, long modulus) 
    {
		String bitString;
		char[] bits;
		long result;
		
		
		bitString = Long.toBinaryString(power);
		bits = bitString.toCharArray();
		result = 1;
		
		for (char bit : bits) 
                {
			result = (result * result) % modulus;
			
			if (bit == '1')
                        {
				result = (result * base) % modulus;
			}
		}
		
		return result;
	}
}